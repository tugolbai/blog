import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Post } from '../shared/post.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  title!: string;
  description!: string;
  headline!: string;
  id!: string;
  preloader = false;
  constructor(public http: HttpClient, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId = params['id'];
      this.id = postId;
      if (this.id) {
        this.headline = 'Edit Post';
      } else {
        this.headline = 'Add new post';
      }
      this.http.get<Post>('https://plovo-57f89-default-rtdb.firebaseio.com/posts/' + postId + '.json')
        .subscribe(post => {
          this.title = post.title;
          this.description = post.description;
        });
    });
  }

  createPost() {
    const title = this.title;
    const description = this.description;
    const date = new Date().toISOString();
    const body = {title, date, description};

    if (this.id) {
      this.preloader = true;
      this.http.put('https://plovo-57f89-default-rtdb.firebaseio.com/posts/' + this.id + '.json', body).subscribe();
      this.http.get<{[id: string]: Post}>('https://plovo-57f89-default-rtdb.firebaseio.com/posts.json').subscribe();
      void this.router.navigate(['/']);
    } else {
      this.preloader = true;
      this.http.post('https://plovo-57f89-default-rtdb.firebaseio.com/posts.json', body).subscribe();
    }

    this.preloader = false;
  }

  getClass() {
    if (this.preloader) {
      return 'displayBlock';
    } else {
      return 'displayNone';
    }
  }
}
