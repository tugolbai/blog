import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../shared/post.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts!: Post[];
  preloader = false;
  constructor( private http: HttpClient) { }

  ngOnInit(): void {
    this.preloader = true;
    this.http.get<{[id: string]: Post}>('https://plovo-57f89-default-rtdb.firebaseio.com/posts.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        } else {
          return  Object.keys(result).map(id => {
            const postData = result[id];
            return new Post(id, postData.title, postData.date, postData.description);
          });
        }
      }))
      .subscribe(posts => {
        this.posts = posts;
        this.preloader = false;
      });
  }

  getClass() {
    if (this.preloader) {
      return 'displayBlock';
    } else {
      return 'displayNone';
    }
  }
}
