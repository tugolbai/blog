import { Component, OnInit } from '@angular/core';
import { Post } from '../../shared/post.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post!: Post;
  id!: string;
  preloader = false;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
    });
    this.getPost();
  }

  getPost() {
    this.preloader = true
    this.http.get<Post>('https://plovo-57f89-default-rtdb.firebaseio.com/posts/' + this.id + '.json')
      .pipe(map(result => {
        return new Post(result.id, result.title, result.date, result.description);
      }))
      .subscribe(post => {
        this.post = post;
        this.preloader = false;
      });
  }

  delete() {
    void this.router.navigate(['/']);
    this.http.delete('https://plovo-57f89-default-rtdb.firebaseio.com/posts/' + this.id + '.json').subscribe();
    this.http.get<{[id: string]: Post}>('https://plovo-57f89-default-rtdb.firebaseio.com/posts.json').subscribe();
  }

  getClass() {
    if (this.preloader) {
      return 'displayBlock';
    } else {
      return 'displayNone';
    }
  }
}
