import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found.component';
import { NewPostComponent } from './new-post/new-post.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { PostDetailsComponent } from './posts/post-details/post-details.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'posts/add', component: NewPostComponent},
  {path: 'posts/:id', component: PostDetailsComponent},
  {path: 'posts/:id/edit', component: NewPostComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
